from row_number import RowNumber


class RowNumberWrapper(object):

    def __init__(self):
        pass

    def _get_number_of_rows(self, extension, root) -> int:
        # call the RowNumber class with a given extension and root
        local_row_number = RowNumber(extension, root)
        return local_row_number.get_rows()

    def open_dialog(self):
        row_numbers = 0
        run = True
        while run:  # all the needed documentation found in the input() functions
            extension = input("Introduce a file extension (without the dot, like: java, py etc): ")
            root = input("Introduce the root file (absolute path): ")
            current_row_numbers = self._get_number_of_rows(extension, root)
            print("From this root directory you had written {} lines".format(current_row_numbers))
            row_numbers += current_row_numbers

            decision = input("Press Enter to finish or Any Key and Enter to continue: ")
            if decision is '':  # enter key is translated to ''
                run = False

        print("From all the root directories you had written {} lines of code! Congratz!".format(row_numbers))


if __name__ == "RowNumberWrapper" or __name__ == "__main__":
    # so it will work also when it's called from outside the script
    local_row_number_wrapper = RowNumberWrapper()
    local_row_number_wrapper.open_dialog()

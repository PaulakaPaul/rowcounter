import os
import fnmatch as matcher
import sys

class RowNumber(object):
    """"Class which gives the number of rows from files in a file tree source"""

    def __init__(self, extension, file_source):
        self._file_source = file_source
        self._extension = extension

    def get_rows(self) -> int:
        row_number = 0
        pattern = "*.{}".format(self._extension)
        for path, directories, files in os.walk(self._file_source, topdown=True):

            if files:  # if there are any files in the current path, directory

                print("SOURCE PATH: " + path) 
                # show the path where are files

                for file in (f for f in files if matcher.fnmatch(f, pattern)):
                    # filters the files with the given extension
                    full_file_path = os.path.join(path, file)  # get the full path to open the file
                    row_number += self._get_number_of_rows(full_file_path)
                    # get the number of rows from the current file

        return row_number

    def _get_number_of_rows(self, file):
        row_number = 0
        try:
            with open(file, "r") as row_file:
                for line in row_file:
                    if line != '\n':  # do not count empty lines
                        row_number += 1
        except FileNotFoundError as err:
            row_number = 0
            print(str(type(err)) + ": " + err.filename)
            print("Resetting row number to 0")
        finally:
            return row_number


if __name__ == '__main__':

    arg_list = sys.argv
    print(len(arg_list))
    if len(arg_list) == 3:
        print(arg_list[0])
        my_row_number = RowNumber(arg_list[1], arg_list[2])
        print(my_row_number.get_rows())
